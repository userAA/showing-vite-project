import Host from './host';

type User = Host;

type ReviewType = {
    id: string;
    date: string;
    user: User;
    comment: string;
    rating: number;
}

type CommentSend = {
    comment: string;
    rating: number;
}

export default ReviewType;
export type {CommentSend};