import React from 'react'
import ReactDOM from 'react-dom/client'
import {Provider} from 'react-redux';

import App from './app.tsx'

import store from './store/index.ts';
import ErrorMessage from './components/error-message/error-message.tsx';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <Provider store={store}>
      <ErrorMessage/>
      <App />
    </Provider>
  </React.StrictMode>,
)
