import {useState, useRef, useEffect} from 'react';

import {Map} from 'leaflet';
import L from 'leaflet';

import Loc from '../types/loc';

function useMap(
    mapRef: React.MutableRefObject<HTMLElement | null>,
    citiLocation: Loc | object
): Map | null {
    const [map, setMap] = useState<Map | null>(null);
    const isRenderedRef = useRef<boolean>(false);

    useEffect(() => {
        if (!isRenderedRef.current && mapRef.current !== null && citiLocation && 'latitude' in citiLocation) {

            const instance = L.map(mapRef.current, {
                center: {
                    lat: citiLocation.latitude,
                    lng: citiLocation.longitude
                },
                zoom: citiLocation.zoom
            })

            const layer = L.tileLayer(
                'https://{s}.basemaps.cartocdn.com/rastertiles/voyager/{z}/{x}/{y}{r}.png',
                {
                  attribution:
                    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'
                }
            );

            layer.addTo(instance);

            setMap(instance);
            isRenderedRef.current = true;
        }
    }, [mapRef, citiLocation])

    return map;
}

export default useMap;